﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
//using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.Core.ExcelPackage;
using OfficeOpenXml.Style;
using OfficeOpenXml;

using Repo.Models.Context;
using Repo.Models.CustumModels;
using Repo.Repository;
using Microsoft.AspNetCore.Hosting;
using DinkToPdf.Contracts;
using DinkToPdf;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Hosting;

namespace AspCore.Controllers
{
    [Authorize]
    [Route("api/SampleCustumer")]
    [ApiController]
    public class SampleCustumerController : ControllerBase
    {
        private readonly ICustumer _CustumerRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConverter _converter;
        private readonly ILoggerFactory _loggerFactory;

        
       
        public SampleCustumerController(ICustumer repository, IHostingEnvironment hostingEnvironment, IConverter converter, ILoggerFactory loggerFactory)
        {
            _CustumerRepository = repository;
            _hostingEnvironment = hostingEnvironment;
            _converter = converter;
            _loggerFactory = loggerFactory;
        }

        //private readonly IHostingEnvironment _hostingEnvironment;
        //public SampleCustumerController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}

        [HttpPost]
        [Route("SortCustumers")]
        public CumCustumer SortCustumers(string data,string sort)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._CustumerRepository.SortCustumerDetails(data,sort);
            return Response;
            
        }
        [HttpPost]
        [Route("Sap")]
        public CumCustumer Sap(searchpage request)

        {
            CumCustumer c = new CumCustumer();
            //var user = await _userManager.GetUserAsync(HttpContext.User);
            try
            {
                
                var Response = this._CustumerRepository.getCustumersRepo(request);
                return Response;
            }
            catch(Exception e)
            {
                var logger = _loggerFactory.CreateLogger("LoggerCategory");
                logger.LogInformation(e.Message);
                return c;
            }
           

        }
        //[HttpPost]
        //[Route("Sap")]
        //public CumCustumer Sap(searchpage request)

        //{
        //    //var user = await _userManager.GetUserAsync(HttpContext.User);
        //    var logger = _loggerFactory.CreateLogger("LoggerCategory");
        //    logger.LogInformation("Calling the ping action");
        //    var Response = this._CustumerRepository.getCustumersRepo(request);
        //    //return Response;
        //    return logger.LogInformation("error", Response);

        //}
        [Route("saveCuDetails")]
        [HttpPost]
        public string saveCuDetails(Custumer request)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._CustumerRepository.saveCustumerDetails(request);
            return Response;

        }
        [Route("getCuDetailsById")]
        [HttpGet]
        public Custumer getCuDetailsById(int Id)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._CustumerRepository.getCustumerDetailsById(Id);
            return Response;

        }
        [Route("updateCuDetails")]
        [HttpPost]
        public string updateCuDetails(Custumer request)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._CustumerRepository.updateCustumerDetails(request);
            return Response;

        }

        [Route("delCuDetailsById")]
        [HttpPost]
        public string delCuDetailsById(int Id)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._CustumerRepository.deleteCustumerDetailsById(Id);
            return Response;

        }
        [Route("ExportExcel")]
        [HttpPost]
        public async Task<DemoResponse<string>> Export(CancellationToken cancellationToken)
        {
            // string folder = _hostingEnvironment.WebRootPath;
            string folder = Directory.GetCurrentDirectory(); 
            string excelName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
            string downloadUrl = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, excelName);
            FileInfo file = new FileInfo(Path.Combine(folder, "wwwroot/FileUploads", excelName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(folder, excelName));
            }

            // query data from database  
            await Task.Yield();

            var list = _CustumerRepository.ExportData();
            MemoryStream stream = new MemoryStream();
            using (OfficeOpenXml.ExcelPackage package = new OfficeOpenXml.ExcelPackage(stream))
            {
            }
            using (var package = new OfficeOpenXml.ExcelPackage(file))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                ;

                workSheet.Cells.LoadFromCollection(list, true);
                package.Save();
            }

            return DemoResponse<string>.GetResult(0, "OK", excelName);
        }
        [Route("ExportPdf")]
        [HttpPost]
        public async Task<DemoResponse<string>> ExportPdf(CancellationToken cancellationToken)
        {
            string pdfName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.pdf";

            //string downloadUrl = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, pdfName);

           
            var sb = new StringBuilder();
            try
            {
                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.Letter,
                    Margins = new MarginSettings { Top = 10, Bottom = 10 },
                    DocumentTitle = "Asset Report",
                    Out = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/FileUploads", pdfName)
                };
                await Task.Yield();


                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,

                    HtmlContent = _CustumerRepository.GetHTMLString(),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
                    HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                    FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Employee Details" }
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                _converter.Convert(pdf);
                byte[] file = _converter.Convert(pdf);

                var iFiles = File(file, "application/pdf");
                
            }
            catch (Exception ex)
            {


                return DemoResponse<string>.GetResult(0, "OK", ex.Message);
            }
            //return returnObj;
            return DemoResponse<string>.GetResult(0, "OK", pdfName);

        }
    }
}
