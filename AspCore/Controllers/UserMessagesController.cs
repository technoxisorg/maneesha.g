﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DinkToPdf.Contracts;
using log4net.Repository.Hierarchy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repo.Models.Context;
using Repo.Repository;

namespace AspCore.Controllers
{
    //[Authorize]
    [Route("api/UserMessages")]
    [ApiController]
    public class UserMessagesController : ControllerBase
    {
        private readonly IChat _Chat;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserMessagesController(IChat repository, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _Chat = repository;
          
        }
       // [Authorize]
        [HttpGet]
        [Route("OpenFile")]
        public ActionResult OpenFile(string FileName,string FileType,string FilePath )
        {
            // Find user by passed id
            var filesPath = _hostingEnvironment.WebRootPath + "\\FileUploads";
            
            byte[] fileBytes = System.IO.File.ReadAllBytes(FilePath+FileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet,FileName);
            
        }
        [HttpPost]
        [Route("sendMessages")]
        public bool sendMessages(UserMessages request)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);

            var Response = this._Chat.saveMessages(request);
            return Response;

        }
        [HttpPost]
        [Route("uploadfiles")]
        public async Task<bool> uploadfiles(IList<IFormFile> files,string userid,string senderId)
        {
            UserMessages UM = new UserMessages();

            var filesPath = _hostingEnvironment.WebRootPath + "\\FileUploads\\";

            foreach (var file in files)
            {
                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName;

                // Ensure the file name is correct
                fileName = fileName.Contains("\\")
                    ? fileName.Trim('"').Substring(fileName.LastIndexOf("\\", StringComparison.Ordinal) + 1)
                    : fileName.Trim('"');

                var fullFilePath = Path.Combine(filesPath, fileName);
                UM.fileName = fileName;
                UM.filePath = filesPath;
                UM.fileType = file.ContentType;
                UM.msgFrom = userid;
                UM.msgTo = senderId;
                if (file.Length <= 0)
                {
                    continue;
                }

                using (var stream = new FileStream(fullFilePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                   
                }
            }
            var Response = this._Chat.saveMessages(UM);

            return true;
        }

       
        [HttpGet]
        [Route("getMessages")]
        public List<UserMessages> getMessages(string userId, string senderId)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);
            var Response = this._Chat.getMessages(userId,senderId);
            return Response;

        }

        [HttpGet]
        [Route("haveSeenMsg")]
        public string haveSeenMsg(string userId, string senderId)

        {
            //var user = await _userManager.GetUserAsync(HttpContext.User);
            var Response = this._Chat.haveSeen(userId, senderId);
            return Response;

        }
    }
}