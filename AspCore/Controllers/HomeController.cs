﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspCore.Models;
using Microsoft.AspNetCore.Authorization;
//using Microsoft.Extensions.Logging;
using NLog;
using Microsoft.Extensions.Logging;

namespace AspCore.Controllers
{
    public class HomeController : Controller
    {
        //[Authorize]
        //private readonly ILogger _logger;
        private readonly ILogger<HomeController> _logger;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {

            _logger.LogInformation("Log message in the About() method");
            return View();
        }
        public IActionResult SignalRWhatsApp()
        {

            _logger.LogInformation("Log message in the About() method");
            return View();
        }

        public IActionResult About()
        {
        

            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult SignalR()
        {
            //ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
