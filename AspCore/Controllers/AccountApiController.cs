﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using AspCore.Data;
using AspCore.Data.Models;
using AspCore.Models;
using AspCore.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Repo.Models;
using Repo.Models.Context;
using Repo.Models.CustumModels;
using Repo.Repository;

namespace AspCore.Controllers
{
    [Authorize]
    [Route("api/AccountApi")]
    [ApiController]
    public class AccountApiController : ControllerBase
    {
        private IChat _Chat;

        private SampleDbContext _context;

        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly UserManager<ApplicationUser> _userManager;

        //private readonly ILogger<Register> _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        //  private readonly ILoggerFactory _loggerFactory;
        private readonly ILogger _logger;

        //  private readonly IEmailSender _emailSender;
        public AccountApiController(UserManager<ApplicationUser> userManager, SampleDbContext context, IChat chat, IHostingEnvironment hostingEnvironment, SignInManager<ApplicationUser> signManager, ILogger<AccountApiController> logger)// ILoggerFactory loggerFactory) ILogger<Register> logger)
        {
            _userManager = userManager;
            _signManager = signManager;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _context = context;
            _Chat = chat;
            //_logger = logger;
            // _emailSender = emailSender;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("Register")]
        public async Task<string> Register(Register model)
        {
            if (ModelState.IsValid)
            {

                Guid activationCode = Guid.NewGuid();
               
                //var user = new ApplicationUser() {
                //    Email = "email@email.com", UserName = "username",
                //    Custumer = new Custumer { CustumerId = "FirstName" } };
               
                //var user = new IdentityUser
                //{
                //    NormalizedUserName = model.Email,
                //    PhoneNumber = model.PhoneNumber,
                //    UserName = model.Email
                //};
                //var result = await _userManager.CreateAsync(user, model.Password);
                var user = new ApplicationUser() { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, EmailConfirmed = false };

                IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                var u = await _userManager.FindByEmailAsync(model.Email);
                
                string Id = user.Id;
                //var result1 = await _userManager.AddToRoleAsync(user, "User");
            
            if (result.Succeeded)
                {
                    //await _signManager.SignInAsync(user, false);
                    //return "true";
                    Sendmail(user.Email, user.Id);

                     return "true";
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                        return error.Description;
                    }
                }


            }
            return "true";
        }
        public string Sendmail(string newmail, string Id)
        {
            Guid activationCode = Guid.NewGuid();
            string folder = _hostingEnvironment.WebRootPath;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = false;
            client.Host = "mail.technodemos.com";
            client.Port = 587;
            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("dev@technodemos.com", "Password@123");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;
            //can be obtained from your model
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("dev@technodemos.com");
            msg.To.Add(new MailAddress(newmail));

            msg.Subject = "Message from A.info";
            msg.IsBodyHtml = true;
            string body = "Hello " + newmail + ",";
            body += "<br /><a href = '" + string.Format("{0}://{1}/{2}/ConfirmMailPage", Request.Scheme, Request.Host, Id) + "'>Click here to activate your account.</a>";
            // msg.Body = string.Format("<html><head></head><body><b>Message Email</b></body>");
            msg.Body = body;
            try
            {
                client.Send(msg);
                return "OK";
            }
            catch (Exception ex)
            {

                return "error:" + ex.ToString();
            }
        }
       [Authorize]
        [HttpPost]
        [Route("Logout")]
        public async Task<string> Logout()
        {
            await _signManager.SignOutAsync();
            return "true";
        }

        [AllowAnonymous]
        [Route("Login")]
        [HttpPost]
        public async Task<Response> Login(Login model)
        {
            //var user = _userManager.(model.Username);
            Response R =new Response();
            var user = await _userManager.FindByEmailAsync(model.Username);
            try
            {
                // try something here
                //throw new Exception();
           
            if (user != null)
            {
                if (user.EmailConfirmed == true)
                {
                    if (ModelState.IsValid)
                    {
                        var result = await _signManager.PasswordSignInAsync(model.Username,
                           model.Password, model.RememberMe, false);

                        if (result.Succeeded)
                        {

                            //Authentication successful, Issue Token with user credentials
                            //Provide the security key which was given in the JWToken configuration in Startup.cs
                             var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("YourKey-2374-OFFKDI940NG7:56753253-tyuw-5769-0921-kfirox29zoxv"));
                                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                                var tokeOptions = new JwtSecurityToken(
                                    issuer: _hostingEnvironment.WebRootPath,
                                    audience: _hostingEnvironment.WebRootPath,
                                    claims: new List<Claim>(),
                                    expires: DateTime.Now.AddDays(3),
                                   // expires: DateTime.UtcNow.AddDays(60),
                                    signingCredentials: signinCredentials
                                );

                                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                            R.token = tokenString;
                            R.data = "Success";
                                R.userId = user.Id;
                            R.res = true;
                              //  return  R;
                            
                            

                        }
                    }
                    return R;

                }
                else
                {

                    R.res=false;
                    R.data = "Please confirm Email";
                    return R;
                }
            }
            else
            {
                R.res = false;
                R.data = "null";
                return R;
            }
            }
            catch (Exception someException)
            {
            
                _logger.LogError(someException.Message);
            
                R.res = false;
                R.data = "null";
                return R;
                // continue handling exception
            }

        }


        //   [Authorize]
        [AllowAnonymous]
        [Route("ConfirmMail")]
        [HttpGet]
        public async Task<string> ConfirmEmail(string Id)
        {

            var user = await _userManager.FindByIdAsync(Id);
            if (user != null)
            {

                user.EmailConfirmed = true;
                await _userManager.UpdateAsync(user);
                //await _signInManager(user, isPersistent: false);
                return "true";

            }
            else
            {
                return "false";
            }

        }
        [Authorize]
        [Route("getUsers")]
        [HttpGet]
        public async Task<List<UsermsgCount>> getUsers(string Id)
        {
            var users =  _userManager.Users.Where(s=>s.Id!=Id).ToList();
            var cuuser = await _userManager.FindByIdAsync(Id);
            //var R = this._Chat.getCount(users, cuuser.Id);
           
            List<getUsers> t = new List<getUsers>();
            List<UsermsgCount> UsermsgC = new List<UsermsgCount>();
            for (int i = 0; i < users.Count; i++)
            {
                UsermsgCount d = new UsermsgCount();
                var y = _context.userMessages.Where(x => x.groupName == cuuser.Id + "/" + users[i].Id || x.groupName == users[i].Id + "/" + cuuser.Id).Where(a=>a.haveSeenReceiver==false).ToList();
                d.totalcount = y.Count();
                d.Email = users[i].Email;
                d.Id = users[i].Id;
                UsermsgC.Add(d);
            }
            if (users != null)
            {
               
                return UsermsgC;

            }
            else
            {
                return null;
            }

        }
        [AllowAnonymous]
       // [Authorize]
        [Route("ForgotPassword")]
        [HttpPost]
        public async Task<string> ForgotPassword(string UserName)
        {

            var user = await _userManager.FindByEmailAsync(UserName);
            if (user != null)
            {
                await Reset(user.Email);
                //user.EmailConfirmed = true;
                //await _userManager.UpdateAsync(user);
                ////await _signInManager(user, isPersistent: false);
                return "true";

            }
            else
            {
                return "null";
            }

        }
        public async Task<string> Reset(string newmail)
        {
            Guid activationCode = Guid.NewGuid();
            var user = await _userManager.FindByEmailAsync(newmail);
            string token = await _userManager.GeneratePasswordResetTokenAsync(user);
            //token = HttpUtility.UrlEncode(token);
            string folder = _hostingEnvironment.WebRootPath;
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = false;
            client.Host = "mail.technodemos.com";
            client.Port = 587;
            // setup Smtp authentication
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("dev@technodemos.com", "Password@123");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;
            //can be obtained from your model
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("dev@technodemos.com");
            msg.To.Add(new MailAddress(newmail));

            msg.Subject = "Message from A.info";
            msg.IsBodyHtml = true;
            string body = "Hello " + newmail + ",";
           var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, token,Request.Scheme);
    //        var link = "<a href='"  
    //+ Request.Scheme + "://"
    //+ Request.Host
    //+ @Url.Action("ResetPassword", "Account", new { email = user.Email, code = token })
    //+ "'>Click here to reset your password</a>";
            // var lnkHref = string.Format("<a href='" + Url.Action("ResetPassword", "Account", new { email = user.Email, code = token }, "http") + "'>Reset Password</a>");
            //body += "<br /><a href = '" + string.Format("{0}://{1}/Account/ResetPassword/{2}", Request.Scheme, Request.Host, newmail) + "'>Click here to Reset Password.</a>";
            // msg.Body = string.Format("<html><head></head><body><b>Message Email</b></body>");
            body += "<b>Please find the Password Reset Link. </b><br/>" + callbackUrl; 
            msg.Body = body;
            try
            {
                client.Send(msg);
                return "OK";
            }
            catch (Exception ex)
            {

                return "error:" + ex.ToString();
            }
        }



        [AllowAnonymous]
       // [Authorize]
        [Route("ResetPassword")]
        [HttpPost]
        public async Task<string> ResetPassword(ResetPassword request)
        {

            var user = await _userManager.FindByIdAsync(request.UserName);
           

                
                try
                {
                if (user != null)
                {

                    var result = await _userManager.ResetPasswordAsync(user, request.Token, request.NewPassword);
                    await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return "true";

                    }
                    //else
                    //{
                    //    return "false";

                    //}
                }
                else
                {
                    return "false";
                }
            }
                 catch (Exception someException)
                {
                    //  var s= _logger.LogInformation("Information is logged");
                    //_logger.LogWarning("Warning is logged");
                    //_logger.LogDebug("Debug log is logged");
                    _logger.LogError(someException.Message);
                    // _logger.LogError(someException, someException.Message,false);
                    return "false";
                    // continue handling exception
                }

            return "";

        }
    }

}