﻿using Microsoft.AspNetCore.Rewrite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCore.Services
{
    public class EmployeeRedirectRule : IRule
    {
        //public class MyCustomRules : Microsoft.AspNetCore.Rewrite.IRule
        //{
            private int StatusCode { get; } = (int)System.Net.HttpStatusCode.MovedPermanently;

            private const string PARAMETER = "parameter=";

            public void ApplyRule(RewriteContext context)
            {
                var request = context.HttpContext.Request;
                var host = request.Host;
                var url = request.Path.Value;
                var queryString = request.QueryString.Value;


                if (queryString.Contains(PARAMETER, StringComparison.OrdinalIgnoreCase))
                {
                    var ho = request.Host;
                    var originalText = queryString.Split(PARAMETER)[1];

                var convertedText = "ss";
                    //processText.Method(originalText);

                    var newUrl = request.Scheme + ho.Value + request.Path + "?" + PARAMETER + convertedText;
                    var response = context.HttpContext.Response;
                    response.StatusCode = StatusCode;
                    response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Location] = newUrl;
                    context.Result = RuleResult.EndResponse;
                    return;
                }

                context.Result = RuleResult.ContinueRules;
                return;
            }
       // }
    }
}
