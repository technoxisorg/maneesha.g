﻿
using System;
using System.Threading.Tasks;
using System.Web;
using AspCore.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Repo.Models.CustumModels;

namespace AspCore.Services
{
    public class ChatHub : Hub
    {
        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ChatHub(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signManager, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
            _signManager = signManager;
           
        }
        private readonly static ConnectionMapping<string> _connections = new ConnectionMapping<string>();

        static List<UserDetail> ConnectedUsers = new List<UserDetail>();
        static List<MessageDetail> CurrentMessage = new List<MessageDetail>();
        private static List<getUsers> users = new List<getUsers>();



        public Task SendMessage(string user, string message, string senderconnId)
        {
            string name = Context.User.Identity.Name;
            string conId = Context.ConnectionId;
             return Clients.All.SendAsync("ReceiveMessage", user, message);
           // Clients.User(user).send(message);    
           // return Clients.Client(connId).appendNewMessage("ReceiveMessage", user, message);
        }
        public void BroadCastMessage(String user, String message, string senderId, String GroupName)
        {
            var id = Context.ConnectionId;
            var Receiver = senderId;
            string[] Exceptional = new string[0];
            Exceptional[0] = id;
            //Clients.Group(GroupName).SendAsync("ReceiveMessage", user, message, id);
            Clients.GroupExcept(GroupName,Exceptional).SendAsync("ReceiveMessage", Receiver, message, id);
        }
        public void SendPrivateMessage(string user, string userId, string message, string senderId, string connId)
        {
            var onlineclient = Clients.Client(Context.ConnectionId);
            var Receiver = senderId;
            //var userPro = user;
            //return Clients.User(Context.ConnectionId).SendAsync("ReceiveMessage", Receiver, message);
            //Clients.All.SendAsync("ReceiveMessage", Receiver, message, senderId);         
            var id = Context.ConnectionId;
            var date = DateTime.Now;

            //Clients.Caller.SendAsync("ReceiveMessage", user, message,senderId);
            //Clients.Client(connId).SendAsync("ReceiveMessage", Receiver, message,id);
            try
            {
                string fromconnectionid = Context.ConnectionId;
                string strfromUserId = (ConnectedUsers.Where(u => u.ConnectionId == Context.ConnectionId).Select(u => u.UserID).FirstOrDefault()).ToString();
                //string _fromUserId = "0";
                //int.TryParse(strfromUserId, out _fromUserId);
                //int _toUserId = 0;
                //int.TryParse(senderId, out _toUserId);
                List<UserDetail> FromUsers = ConnectedUsers.Where(u => u.UserID == userId).ToList();
                List<UserDetail> ToUsers = ConnectedUsers.Where(x => x.UserID == senderId).ToList();

                if (FromUsers.Count != 0 || ToUsers.Count() != 0)
                {
                    foreach (var ToUser in ToUsers)
                    {
                        // send to                                                                                            //Chat Title
                        Clients.Client(ToUser.ConnectionId).SendAsync("ReceiveMessage", senderId, message, userId, date);
                    }


                    foreach (var FromUser in FromUsers)
                    {
                        // send to caller user                                                                                //Chat Title
                        Clients.Client(FromUser.ConnectionId).SendAsync("ReceiveMessage", senderId, message, userId, date);
                    }
                    // send to caller user
                    //Clients.Caller.sendPrivateMessage(_toUserId.ToString(), FromUsers[0].UserName, message);
                    //ChatDB.Instance.SaveChatHistory(_fromUserId, _toUserId, message);
                    MessageDetail _MessageDeail = new MessageDetail { FromUserID = user, FromUserName = FromUsers[0].UserName, ToUserID = senderId, ToUserName = ToUsers[0].UserName, Message = message };
                    AddMessageinCache(_MessageDeail);
                }
            }
            catch { }
        }

        public void SendFileMessage(string user, string userId, string filename,string filetype, string senderId, string connId)
        {
            var onlineclient = Clients.Client(Context.ConnectionId);
            var Receiver = senderId;
            var filesPath = _hostingEnvironment.WebRootPath + "\\FileUploads\\";

            var id = Context.ConnectionId;
            var date = DateTime.Now;
            try
            {
                string fromconnectionid = Context.ConnectionId;
                string strfromUserId = (ConnectedUsers.Where(u => u.ConnectionId == Context.ConnectionId).Select(u => u.UserID).FirstOrDefault()).ToString();
                
                List<UserDetail> FromUsers = ConnectedUsers.Where(u => u.UserID == userId).ToList();
                List<UserDetail> ToUsers = ConnectedUsers.Where(x => x.UserID == senderId).ToList();

                if (FromUsers.Count != 0 || ToUsers.Count() != 0)
                {
                    foreach (var ToUser in ToUsers)
                    {
                        // send to                                                                                            //Chat Title
                        Clients.Client(ToUser.ConnectionId).SendAsync("ReceivefileMessage", senderId, filename, filetype, filesPath, date, userId);
                    }


                    foreach (var FromUser in FromUsers)
                    {
                        // send to caller user                                                                                //Chat Title
                        Clients.Client(FromUser.ConnectionId).SendAsync("ReceivefileMessage", senderId, filename, filetype, filesPath, date, userId);
                    }
                    
                    MessageDetail _MessageDeail = new MessageDetail { FromUserID = user, FromUserName = FromUsers[0].UserName, ToUserID = senderId, ToUserName = ToUsers[0].UserName};
                    AddMessageinCache(_MessageDeail);
                }
            }
            catch { }
        }

        private void AddMessageinCache(MessageDetail _MessageDetail)
        {
            CurrentMessage.Add(_MessageDetail);
            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }
        public  void Get_Connect(String username, String userid, String connectionid, String GroupName)
        {
            var id = Context.ConnectionId;

            if (ConnectedUsers.Count(x => x.ConnectionId == id) == 0)
            {
                ConnectedUsers.Add(new UserDetail { ConnectionId = id, UserName = username + "-" + userid, UserID = userid });
            }
            UserDetail CurrentUser = ConnectedUsers.Where(u => u.ConnectionId == id).FirstOrDefault();

            string[] Exceptional = new string[1];
            Exceptional[0] = id;
            Clients.AllExcept(Exceptional).SendAsync("NewConnection", GroupName + " " + username + " " + id);
        }
        public string GetConnectionId(String userId)
        {
            getUsers d = new getUsers();
            d.ConnectionId = Context.ConnectionId;
            d.UserId = userId;
            users.Add(d);
            Clients.All.SendAsync("ReceiveConnectionIds", users);         
            return Context.ConnectionId;
        }
        public Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToGroup(string message)
        {
            return Clients.Group("SignalR Users").SendAsync("ReceiveMessage", message);
        }

        #region HubMethodName
        [HubMethodName("SendMessageToUser")]
        public Task DirectMessage(string user, string message)
        {
            string name = Context.User.Identity.Name;
            return Clients.User(user).SendAsync("ReceiveMessage", message);
        }
        #endregion

        #region ThrowHubException
        public Task ThrowException()
        {
            throw new HubException("This error will be sent to the client!");
        }
        #endregion

        #region OnConnectedAsync
        public override async Task OnConnectedAsync()
        {
           // users.Add(Context.ConnectionId);
            await Groups.AddToGroupAsync(Context.ConnectionId, "SignalR Users");
            await base.OnConnectedAsync();
        }
        #endregion

        #region OnDisconnectedAsync
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            //users.Remove(Context.ConnectionId);
            for(int i = 0; i < users.Count; i++)
            {
                if(users[i].ConnectionId== Context.ConnectionId)
                {
                    users.Remove(users[i]);
                }
            }
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "SignalR Users");
            await base.OnDisconnectedAsync(exception);
        }
        #endregion
    }
}