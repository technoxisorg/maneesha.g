﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using AspCore.Entities;
using System.Net;
using System.Net.Mail;

namespace AspCore.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            //IOptions<emailsettings>
            _emailSettings = emailSettings.Value;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                // Credentials
                var credentials = new NetworkCredential(_emailSettings.Sender, _emailSettings.Password);

                // Mail message
                var mail = new MailMessage()
                {
                    From = new MailAddress(_emailSettings.Sender, _emailSettings.SenderName),
                    Subject = subject,
                    Body = message,
                    IsBodyHtml = true
                };

                mail.To.Add(new MailAddress(email));

                // Smtp client
                var client = new SmtpClient()
                {
                    Port = _emailSettings.MailPort,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = _emailSettings.MailServer,
                    EnableSsl = true,
                    Credentials = credentials
                };

                // Send it...         
                client.Send(mail);
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }

            return Task.CompletedTask;
        }
        //    public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        //    {
        //        Options = optionsAccessor.Value;
        //    }

        //    public AuthMessageSenderOptions Options { get; } //set only via Secret Manager

        //    public Task SendEmailAsync(string email, string subject, string message)
        //    {
        //        return Execute(Options.SendGridKey, subject, message, email);
        //    }

        //    public Task Execute(string apiKey, string subject, string message, string email)
        //    {
        //        var client = new SendGridClient(apiKey);
        //        var msg = new SendGridMessage()
        //        {
        //            From = new EmailAddress("Joe@contoso.com", Options.SendGridUser),
        //            Subject = subject,
        //            PlainTextContent = message,
        //            HtmlContent = message
        //        };
        //        msg.AddTo(new EmailAddress(email));

        //        // Disable click tracking.
        //        // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
        //        msg.SetClickTracking(false, false);

        //        return client.SendEmailAsync(msg);
        //    }
        //}
    }
}

