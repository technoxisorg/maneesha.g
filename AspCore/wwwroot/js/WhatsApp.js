﻿var GenerateToken = JSON.parse(localStorage.getItem('token')) || [];
var senderId;
var html;
var html3;
var senderemail;
var usr = JSON.parse(localStorage.getItem('userCredentials')) || [];
$(document).ready(
    // getdata(),
    $('#action_menu_btn').click(function () {
        $('.action_menu').toggle();
    }),
    $(".heading-compose").click(function () {
        $(".side-two").css({
            "left": "0"
        });
    }),
    $(".newMessage-back").click(function () {
        $(".side-two").css({
            "left": "-100%"
        });
    }),
    html3 = "",
    html3 += '<a class="heading-name-meta">' + usr.username + '</a>',
    $("#displayUserId").empty().append(html3),
    document.getElementById("sendMessage").addEventListener("click", event => {
        sendmessages();
    }),

    $(".conversation").css("display","none"),
       
);
$("i.fa-paperclip").click(function () {
    $("input[type='file']").val(null);
    $("input[type='file']").trigger('click');
});

$('input[type="file"]').on('change', function () {
    var files = document.getElementById('myfile').files[0];
    uploadFile(files);
});
$(document).on('click', '.row .sideBar-body', function () {
    $(".row .sideBar-body").addClass('active');
    $(".sideBar-body").css("background-color", "");
    $(this).css("background-color", "#ff99cc");
    //$(".row .sideBar-body .active").css("background-color", "red");
    $(".conversation").css("display", "");
    senderId = $(this).attr('id');
    senderemail = $(this).attr('value');
    const userId = JSON.parse(localStorage.getItem('userId')) || [];
    var html2 = "";
    html2 += '<a class="heading-name-meta">' + senderemail + '</a>';
    $("#displaysenderId").empty().append(html2);
    var rUrl = RootUrl + "/api/UserMessages/getMessages?userId=" + userId.userId + "&senderId=" + senderId;
    $.ajax({
        type: 'GET',
        url: rUrl,
        //headers: { Authorization: "bearer " + GenerateToken },
        //datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            haveSeen(userId.userId, senderId );
            //getusers();

             html = "";
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    var Date;
                    var D;
                    var Time;
                    if (data[i].msgFrom == userId.userId) {
                        Date = data[i].date.split("T");
                        D = Date[0].split("-");
                        Time = Date[1].split(".");
                        if (data[i].message != null && data[i].message != '') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += data[i].message;
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'image/gif' || data[i].fileType == 'image/jpeg' || data[i].fileType == 'image/jpg' || data[i].fileType == 'image/png') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./FileUploads" + '/' + data[i].fileName + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        //else if (data[i].FileType == 'application/octet-stream' application/vnd.openxmlformats-officedocument.spreadsheetml.sheet) {
                        //}
                        else if (data[i].fileType == 'application/zip' || data[i].fileType =='application/x-zip-compressed') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/zip.jpg" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        //else if (data[i].FileType == 'application/x-msdownload') {
                        //}
                        else if (data[i].fileType == 'application/pdf') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/pdf.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'text/plain') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/text.jpg" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'spreadsheetml.sheet' || data[i].fileType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/spreadsheet.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'application/msword' || data[i].fileType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/msword.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'application/vnd.ms-excel' || data[i].fileType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-sender">';
                            html += '<div class="sender">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/excel.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    } else {
                        Date = data[i].date.split("T");
                        D = Date[0].split("-");
                        Time = Date[1].split(".");
                        if (data[i].message != null && data[i].message != '') {

                          
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += data[i].message;
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';;
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'image/gif' || data[i].fileType == 'image/jpeg' || data[i].fileType == 'image/jpg' || data[i].fileType == 'image/png') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./FileUploads" + '/' + data[i].fileName + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        //else if (data[i].filetype == 'application/octet-stream') {
                        //}
                        else if (data[i].fileType == 'application/zip' || data[i].fileType == 'application/x-zip-compressed') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/zip.jpg" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        //else if (data[i].filetype == 'application/x-msdownload') {
                        //}
                        else if (data[i].fileType == 'application/pdf') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/pdf.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'text/plain') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/text.jpg" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'spreadsheetml.sheet' || data[i].fileType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/spreadsheet.png" + '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                        else if (data[i].fileType == 'application/msword' || data[i].fileType =='application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                        html += '<div class="message-text">';
                        html += '<img src="'  + "./images/msword.png"+ '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                        html += '</div>';
                        html += '<span class="message-time pull-right">';
                        html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                        html += '</span>';
                        html += '</div>';
                        html += '</div>';
                        html += '</div>';
                        }
                        else if (data[i].fileType == 'application/vnd.ms-excel' || data[i].fileType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            html += '<div class="row message-body">';
                            html += '<div class="col-sm-12 message-main-receiver">';
                            html += '<div class="receiver">';
                            html += '<div class="message-text">';
                            html += '<img src="'  + "./images/excel.png" +  '" style="cursor: pointer;float:left!important;width:75px;height:80px;padding-right:0px !important;padding-left:0px !important"  style="float:right!important" class="chat-image col-lg-6" id="' + data[i].filePath + '$' + data[i].fileName + '$' + data[i].fileType + '" onclick="downloadfile(this)" alt="" >';
                            html += '</div>';
                            html += '<span class="message-time pull-right">';
                            html += '<span style="" class="today small">' + D[2] + "-" + D[1] + "-" + D[0] + "  " + Time[0] + '</span>';
                            html += '</span>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    }

                    $("#conversation").empty().append(html);
                }


                $('#conversation').animate({
                    scrollTop: $('#conversation')[0].scrollHeight
                }, 2000);
            }
            else {
                $("#conversation").empty();
            }



        },
        error: function (jqXHR, textStatus, errorThrown) {
            Redirect(jqXHR, textStatus, errorThrown);

        }

    });
});
function downloadfile($this) {
    var id = $this.id;

    var details = id.split("$");
  
    var token = JSON.parse(localStorage.getItem('token')) || [];
    var userid = token.userProfileStringId;

    var model = new FormData();
    model.append('FileName', details[1]);
    model.append('FilePath', details[0]);
    model.append('FileType', details[2]);
    model.append('userId', userid);
   //var pageUrl = RootUrl + "api/ProfileDetails/OpenFile?FileName=" + details[1] + "&FileType=" + details[2] + "&FilePath=" + details[0];

    var pageUrl = RootUrl + "/api/UserMessages/OpenFile?FileName=" + details[1] + "&FileType=" + details[2] + "&FilePath=" + details[0];
    window.location.href =  pageUrl;

   
}
$(document).keypress(function (e) {
    var pathname = window.location.pathname;
    var PN = pathname.split('/');
    var Path = PN.length;
    if (e.keyCode == 13) {
        if (PN[Path - 1] == "SignalRWhatsApp") {
            sendmessages();
        }
    }

});
function sendmessages() {
    const message = document.getElementById("message").value;
   // const message = document.getElementById("messageInput").value;
   // const user = document.getElementById("userInput").value;
    const user = JSON.parse(localStorage.getItem('userCredentials')) || [];
    const userId = JSON.parse(localStorage.getItem('userId')) || [];
    //const senderId = $('#ConnectionId').val();
    $(".lsx-emojipicker-container").css("display", "none");
    var connectionId = sessionStorage.getItem('conectionId');

    var GroupName = userId.userId +"/"+ senderId;

    connection.invoke("Get_Connect", user.username, userId.userId, connectionId, GroupName).catch(err => console.error(err));

    var e = document.getElementById("ConnectionId");
    $('#message').val("");
    $('#message').html("");
    //var strUser = e.options[e.selectedIndex].value;
    //var LocalStorageUser = JSON.parse(localStorage.getItem('userCredentials')) || [];
    // const user = userId;
    // $('#userInput').val(LocalStorageUser.username);
    if (senderId != null || senderId != undefined) {
        var request = {
            msgFrom: userId.userId,
            msgTo: senderId,
            groupName: GroupName,
            message: message

        };


        //connection.invoke("SendPrivateMessage", user, userId.userId, message, senderId, connectionId).catch(err => console.error(err));
        connection.invoke("SendPrivateMessage", user.username, userId.userId, message, senderId, connectionId).then(function () {
            $.ajax({
                type: 'POST',
                url: RootUrl + "/api/UserMessages/sendMessages",
                datatype: 'JSON',
                data: JSON.stringify(request),
                headers: { Authorization: "bearer " + GenerateToken },
                contentType: 'application/json; charset=utf-8',
                success: function (Response) {
                    var html;
                    var html2;
                    var c = 1;
                    getusers();

                    //haveSeen(userId.userId, senderId);
                    $('#message').val("");
                    $('#message').html("");

                },
                error: function (jqXHR, textStatus, errorThrown) {

                    Redirect(jqXHR, textStatus, errorThrown);

                }

            });
        }).catch(err => console.error(err.toString()));

        event.preventDefault();
    }
    else {
        Swal.fire('Sender Was Not Defined');
    }
}

function haveSeen(us, sen) {
    debugger;
    const userId = JSON.parse(localStorage.getItem('userId')) || [];
    var rUrl = RootUrl + "/api/UserMessages/haveSeenMsg?userId=" + us + "&senderId=" + sen;
    $.ajax({
        type: 'GET',
        url: rUrl,
        datatype: 'JSON',
        headers: { Authorization: "bearer " + GenerateToken },
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            getusers();

            $('#message').val("");
            $('#message').html("");

        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }

    });

}
function uploadFile(files) {
   // var file = document.getElementById('myfile').files[0];
    var file = files;
    const user = JSON.parse(localStorage.getItem('userCredentials')) || [];
    const userId = JSON.parse(localStorage.getItem('userId')) || [];
    //const senderId = $('#ConnectionId').val();
    $(".lsx-emojipicker-container").css("display", "none");
    var connectionId = sessionStorage.getItem('conectionId');

    var GroupName = userId.userId + "/" + senderId;

    connection.invoke("Get_Connect", user.username, userId.userId, connectionId, GroupName).catch(err => console.error(err));
    if (senderId != null || senderId != undefined) {
        var e = document.getElementById("ConnectionId");
        var model = new FormData();
        //model.append('files', file, file.name);
        //model.append('SenderId', senderId);
        //model.append('userId', userId.userId);
        //model.append('groupName', GroupName);
        //model.append('FileSize', file.size);

        model.append("files", file);
        var MaxContentLength = 1024 * 1024 * 25;
        if (file.size > MaxContentLength) {
            swal("File size should not be greater than 25mb", "error");

        }
        else {
            model.append('haveseen', false);






                $.ajax({
                    url: RootUrl + "/api/UserMessages/uploadfiles?userid=" + userId.userId + "&senderId=" + senderId,
                    type: 'POST',
                    dataType: 'json',
                    data: model,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        connection.invoke("SendFileMessage", user.username, userId.userId, file.name, file.type, senderId, connectionId).then(function () {
                        }).catch(err => console.error(err.toString()));

                        event.preventDefault();
                        // details2(data.responseJSON)

                    },
                    function(jqXHR, textStatus, errorThrown) {

                        Redirect(jqXHR, textStatus, errorThrown);
                    }
                });
           

        }
    } 
}