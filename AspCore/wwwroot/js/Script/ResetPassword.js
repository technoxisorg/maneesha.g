﻿var token;
$(document).ready(
    getUserName(),
    $("#btnsubmit").on("click", function () {
        resetPassword();


    })
);
function getUserName() {
    var url = window.location.href;
    var s = url.substring(url.lastIndexOf('userId'));
    var res = s.split("&code=");
    var sp = res[0].split("userId=");
    var user = sp[1];
    token = res[1];
    $('#txtUserName').val(user);
}
function clear() {

    $('#txtUserName').val("");
    $('#txtNewPassword').val("");
    $('#txtConfirmPassword').val("");

}
function resetPassword() {
    debugger;
    var url = window.location.pathname;
   // var user = url.substring(url.lastIndexOf('/') + 1);
   // var token = url.substring(url.lastIndexOf('%') + 1);
     //token = url.substring(url.lastIndexOf('&code') + 6);
    var Token = JSON.parse(localStorage.getItem('token')) || [];

    var request = {
        UserName: $("#txtUserName").val(),
        NewPassword: $("#txtNewPassword").val(),
        ConfirmPassword: $("#txtConfirmPassword").val(),
        Token:token
    };
    //var url = window.location.pathname;
    //var id = url.substring(url.lastIndexOf('%') + 1);
    //Id = id;
    var rUrl = RootUrl + "/api/AccountApi/ResetPassword";

    $.ajax({
        type: 'POST',
        url: rUrl,
        data: JSON.stringify(request),
       // headers: { Authorization: "bearer " + Token },
        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            clear();
            Swal.fire({
                title: 'Your Password has been changed successfully',
                text: "Please click here to login",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                //cancelButtonColor: '#d33',
                //confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    pageUrl = RootUrl + "LoginPage";
                    window.location.href = pageUrl;
                }
            });

        },
        error: function (response) {
            console.log(response);
        },

    });
}