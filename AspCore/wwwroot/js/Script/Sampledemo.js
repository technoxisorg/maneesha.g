﻿var tokRole;
var GenerateToken = JSON.parse(localStorage.getItem('token')) || [];
var Id;
var DeleteId;

$(document).ready(
    //alert("hi"),
    getdat()
   //getCustumersList()
   //tokRole = JSON.parse(localStorage.getItem('token')) || [],
//UserRole = tokRole.userRole,
);
function Redirect(jqXHR, textStatus, errorThrown) {
    var SuccessUrl;
    if (errorThrown == "Unauthorized") {
        SuccessUrl = RootUrl + "LoginPage";
        window.location.href = SuccessUrl;
    }
    if (textStatus == "error") {
         SuccessUrl = RootUrl + "LoginPage";
        window.location.href = SuccessUrl;
    }
    else {

        Swal.fire(
            'Error!',
            errorThrown,
            'Error'
        )
    }
}
function getdat() {
 var LocalStorageUser = JSON.parse(localStorage.getItem('userCredentials')) || [];
    if (LocalStorageUser.length == 0) {
        if (LocalStorageUser.username == "" || LocalStorageUser.username == undefined) {
            var paUrl = RootUrl + "/LoginPage";
            window.location.href = paUrl;
        }

    } else {
        getCustumersList();
    }
    
}
function sortCustumerslist(data) {
    var str = data.split(",");
    var rUrl = RootUrl + "/api/SampleCustumer/SortCustumers?data=" + str[0] + "&sort=" + str[1];
    $.ajax({
        type: 'POST',
        url: rUrl,
        datatype: 'JSON',
        headers: { Authorization: "bearer " + GenerateToken },
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            var html;
            var html2;
            var c = 1;
            data = Response.cusDetails;

            html2 += 'Showing <b>' + Response.count + '</b> out of <b>' + Response.totalcount + '</b> entries';
            $(".hint-text").empty().append(html2);

            for (var i = 0; i < data.length; i++) {
                html += '<tr>';

                html += '<td>' + data[i].id + '</td>';
                html += '<td>' + data[i].contactName + '</td>';
                html += '<td>' + data[i].contactTitle + '</td>';
                html += '<td>' + data[i].address + '</td>';
                html += '<td>' + data[i].city + '</td>';
                html += '<td>' + data[i].region + '</td>';
                html += '<td>' + data[i].country + '</td>';
                html += '<td>' + data[i].phone + '</td>';
                html += '<td>';
                html += '<a href="#editEmployeeModal" onclick="EditDetailsById(' + data[i].id + ')" class="edit" data-toggle="modal"><i class="material-icons getdetails" id=' + data[i].id + ' data-toggle="tooltip" title="Edit">&#xE254;</i></a>';
                html += '<i style="cursor: pointer;color: red" onclick = "DeleteDetailsById(' + data[i].id + ')" class="material-icons">delete</i>';
                // '<a href="#" class="delete" onclick = "DeleteDetailsById(' + data[i].id + ')" "><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>';
                html += '</td>';
                html += '</tr>';

                $("#tableData").empty().append(html);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }
    });


}
$("#searchData").keyup(function () {
    getCustumersList(1);
});
function getCustumersList(pageIndex) {
    var rUrl = RootUrl;
    var serdata = $('#searchData').val();
    // Get the existing data
    //var userdata = JSON.parse(localStorage.getItem('token')) || [];

    //userdata = JSON.stringify({ token:"dasdsdadasdsda" });
    //localStorage.setItem("token", userdata);


    if (pageIndex == 0 || pageIndex == undefined) {
        pageIndex = 1;
    }
    
    var request = {
        searchdata: serdata,
        pageIndex: pageIndex,
        pageSize: 5
    };
    $.ajax({
        type: 'POST',
        url: RootUrl + "/api/SampleCustumer/Sap",
        datatype: 'JSON',
        data: JSON.stringify(request),
        headers: { Authorization: "bearer " + GenerateToken },
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            var html="";
            var html2="";
            var c = 1;
            data = Response.cusDetails;
            
            html2 += 'Showing <b>' + Response.count + '</b> out of <b>' + Response.totalcount + '</b> entries';
            $(".hint-text").empty().append(html2);
            for (var i = 0; i < data.length; i++){
        html += '<tr>';
                
                html += '<td>' + data[i].id + '</td>';
                html += '<td>' + data[i].contactName+'</td>';
                html += '<td>' + data[i].contactTitle +'</td>';
                html += '<td>' + data[i].address +'</td>';
                html += '<td>' + data[i].city + '</td>';
                html += '<td>' + data[i].region + '</td>';
                html += '<td>' + data[i].country + '</td>';
                html += '<td>' + data[i].phone + '</td>';
                html += '<td>';
                html += '<a href="#editEmployeeModal" onclick="EditDetailsById(' + data[i].id + ')" class="edit" data-toggle="modal"><i class="material-icons getdetails" id=' + data[i].id +' data-toggle="tooltip" title="Edit">&#xE254;</i></a>';
                html += '<i style="cursor: pointer;color: red" onclick = "DeleteDetailsById(' + data[i].id + ')" class="material-icons">delete</i>';
               // '<a href="#" class="delete" onclick = "DeleteDetailsById(' + data[i].id + ')" "><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>';
                html += '</td>';
                html += '</tr>';

                $("#tableData").empty().append(html);
    }
        },
        error: function (jqXHR, textStatus, errorThrown) {
           
            Redirect(jqXHR, textStatus, errorThrown);

        }
       
    });

}
function saveCustumerDetails() {
        var name = $('#contactName').val();
        var contactTitle = $('#contactTitle').val();
        var address = $('#address').val();
        var city = $('#city').val();
        var region = $('#region').val();
        var country = $('#country').val();
        var phone = $('#phone').val();
    var rUrl = RootUrl;
    $('#addEmployeeModal').modal('hide');
    var request = {
        contactName: name,
        contactTitle: contactTitle,
        Address: address,
        city: city,
        region: region,
        country: country,
        phone: phone

    };
    $.ajax({
        type: 'POST',
        url: RootUrl + "/api/SampleCustumer/saveCuDetails",
        headers: { Authorization: "bearer " + GenerateToken },
        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(request),
        success: function (data) {
            getCustumersList();
            var name = $('#contactName').val("");
            var contactTitle = $('#contactTitle').val("");
            var address = $('#address').val("");
            var city = $('#city').val("");
            var region = $('#region').val("");
            var country = $('#country').val("");
            var phone = $('#phone').val("");
            Swal.fire('Details saved successfully');
                        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }

    });
}

function EditDetailsById(id) {
    Id = id;
    var rUrl = RootUrl + "/api/SampleCustumer/getCuDetailsById?Id=" + Id;

    $.ajax({
        type: 'GET',
        url: rUrl,
        headers: { Authorization: "bearer " + GenerateToken },
        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var name = $('#upcontactName').val(data.contactName);
            var contactTitle = $('#upcontactTitle').val(data.contactTitle);
            var address = $('#upaddress').val(data.address);
            var city = $('#upcity').val(data.city);
            var region = $('#upregion').val(data.region);
            var country = $('#upcountry').val(data.country);
            var phone = $('#upphone').val(data.phone);
        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }

    });
}

    
function UpdateDetails(){
    var rUrl = RootUrl;
            var name = $('#upcontactName').val();
            var contactTitle = $('#upcontactTitle').val();
            var address = $('#upaddress').val();
            var city = $('#upcity').val();
            var region = $('#upregion').val();
            var country = $('#upcountry').val();
            var phone = $('#upphone').val();
    $('#editEmployeeModal').modal('hide');
        var request = {
            Id: Id,
            contactName: name,
            contactTitle: contactTitle,
            Address: address,
            city: city,
            region: region,
            country: country,
            phone: phone

        };
        $.ajax({
            type: 'POST',
            url: RootUrl + "/api/SampleCustumer/updateCuDetails",
             headers: { Authorization: "bearer " + GenerateToken },
            datatype: 'JSON',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(request),
            success: function (data) {
                getCustumersList();
                var name = $('#upcontactName').val("");
                var contactTitle = $('#upcontactTitle').val("");
                var address = $('#upaddress').val("");
                var city = $('#upcity').val("");
                var region = $('#upregion').val("");
                var country = $('#upcountry').val("");
                var phone = $('#upphone').val("");
                Swal.fire('Details updated successfully');

            },
            error: function (jqXHR, textStatus, errorThrown) {

                Redirect(jqXHR, textStatus, errorThrown);

            }
        });
    
}


function DeleteDetailsById(delId) {
    var rUrl = RootUrl + "/api/SampleCustumer/delCuDetailsById?Id=" + delId;
    Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: 'POST',
                url: rUrl,
                headers: { Authorization: "bearer " + GenerateToken },

                datatype: 'JSON',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    getCustumersList();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                },
                error: function (jqXHR, textStatus, errorThrown) {

                    Redirect(jqXHR, textStatus, errorThrown);

                }

            });

        }
    });
   
    

}



function exportedExcel(e) {
    var rUrl = RootUrl + "/api/SampleCustumer/ExportExcel";

    $.ajax({
        type: 'POST',
        url: rUrl,
        headers: { Authorization: "bearer " + GenerateToken },

        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            if (Response.data) {
                window.location.href = "FileUploads/"+ Response.data;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }
    });
}



function exportedPdf(e) {
    var rUrl = RootUrl + "/api/SampleCustumer/ExportPdf";

    $.ajax({
        type: 'POST',
        url: rUrl,
        headers: { Authorization: "bearer " + GenerateToken },

        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            if (Response.data) {
                window.location.href = "FileUploads/" + Response.data;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

            Redirect(jqXHR, textStatus, errorThrown);

        }

    });
}


function LogoutPage() {
    //  $("#loginLoader").attr("aria-busy", true);
   var Token = JSON.parse(localStorage.getItem('token')) || [];
    var rUrl = RootUrl + "/api/AccountApi/Logout";
    //localStorage.clear();
    $.ajax({
        type: 'POST',
        url: rUrl,
       // datatype: 'JSON',
       // contentType: 'application/json; charset=utf-8',
       headers: { Authorization: "bearer " + Token },
        success: function (Response) {
            localStorage.clear();
            var pageUrl = RootUrl + "/LoginPage";
            window.location.href = pageUrl;
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //swal(errorThrown, "error")
            console.log(response);
           // Redirect(jqXHR, textStatus, errorThrown);
        }
    })
}