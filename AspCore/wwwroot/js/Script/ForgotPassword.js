﻿
$(document).ready(
    // getdata(),
    localStorage.clear(),
    $("#btnsubmit").on("click", function () {
        forgotPassword();

    })
);
function clear() {

    $('#txtEmailId').val("");
   
}




function forgotPassword() {
    
    //var request = {
    var username = $("#txtEmailId").val();
    var Token = JSON.parse(localStorage.getItem('token')) || [];

    //};
    var rUrl = RootUrl + "/api/AccountApi/ForgotPassword?UserName=" + username;
    $.ajax({
        type: 'POST',
       // headers: { Authorization: "bearer " + Token },
        url: rUrl,
        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            if (Response != "null") {
                if (Response == "true") {
                    clear();
                    Swal.fire({
                        title: 'Your Reset Password Link Has Been sent to your mail',
                        text: "Please click here to login",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        //cancelButtonColor: '#d33',
                        //confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            pageUrl = RootUrl + "LoginPage";
                            window.location.href = pageUrl;
                        }
                    });

                } else {
                    Swal.fire(
                        'Forgot Password!',
                        'Please Check the mail you have entered',
                        'warning'
                    )
                }
            }
            else {
                Swal.fire(
                    'Login Error!',
                    'Email Not Found',
                    'error'
                )
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            //swal("", "The username and password and you have entered is in correct", "error");


        }
    });

}