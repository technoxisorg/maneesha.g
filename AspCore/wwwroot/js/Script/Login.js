﻿
$(document).ready(
   // getdata(),
    localStorage.clear(),
    $("#btnLog").on("click", function () {
        LoginUser();

    })
);
function getdata() {
    var LocalStorageUser = JSON.parse(localStorage.getItem('userCredentials')) || [];
    if (LocalStorageUser.length != 0) {
        if (LocalStorageUser.username != "" || LocalStorageUser.username != undefined) {
            var pageUrl = RootUrl + "/HomePage";
            window.location.href = pageUrl;
        }
       
    }
   
}

function clear() {

    $('#txtLogUserName').val("");
    $('#txtLogPassword').val("");
    document.getElementsByName('Rem').checked = false;
}


var LoginUser = function () {
    debugger;
    var RememberMe;
    if ($("#chkRemember").prop("checked")) {
         RememberMe = true;
    } else {
         RememberMe = false;

    }
    var request = {
        username: $("#txtLogUserName").val(),
        password: $("#txtLogPassword").val(),
        RememberMe: RememberMe
    };
    var rUrl = RootUrl + "/api/AccountApi/Login";
    $.ajax({
        type: 'POST',

        //headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        //dataType: "json",
        url: rUrl,
        datatype: 'JSON',
        //data: $.param({ grant_type: 'password', username: $("#txtLogUserName").val(), password: $("#txtLogPassword").val() }),

        data: JSON.stringify(request),
        //headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        contentType: 'application/json; charset=utf-8',
        success: function (Response) {
            debugger;
            var userCredentials;
            if (Response.data != "null") {
                if (Response.res == true) {
                    if ($("#chkRemember").prop("checked")) {
                         userCredentials = JSON.stringify({
                            "username": $("#txtLogUserName").val(),
                            "password": $("#txtLogPassword").val()
                        })

                        localStorage.setItem("userCredentials", userCredentials);
                        localStorage.setItem("token", JSON.stringify(Response.token));
                    }
                    else {
                        if (localStorage.getItem("userCredentials")) {
                            localStorage.removeItem("userCredentials");
                        }
                    }
                    localStorage.setItem("token", JSON.stringify(Response.token));

                     userCredentials = JSON.stringify({
                         "username": $("#txtLogUserName").val(),

                    });
                    localStorage.setItem("userCredentials", userCredentials);
                    userId = JSON.stringify({
                        "userId": Response.userId

                    });
                    localStorage.setItem("userId", userId);
                    clear();
                    pageUrl = RootUrl + "/HomePage";
                    window.location.href = pageUrl;
                } else {
                    Swal.fire(
                        'Login Error!',
                        'Please Confirm Your Mail',
                        'warning'
                    );
                }
            }
            else {
                Swal.fire(
                    'Login Error!',
                    'Email Not Found',
                    'error'
                )
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
                //swal("", "The username and password and you have entered is in correct", "error");


            }
        });

    
}

