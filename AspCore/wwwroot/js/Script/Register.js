﻿$(document).ready(
    //alert("hi"),
    localStorage.clear(),
    //$('#txtpassInp').passwordstrength({
    //    'minlength': 6,
    //    'number': true,
    //    'capital': true,
    //    'special': true,
    //    'labels': {
    //        'general': 'The password must have :',
    //        'minlength': 'At leaset {{minlength}} characters',
    //        'number': 'At least one number',
    //        'capital': 'At least one uppercase letter',
    //        'special': 'At least one special character'
    //    }
    //}),


      $("#btnRegis").validate({
          ignore: "",
          rules: {
              txtemailInp: {
                  required: true,
              },
              txtpassInp: {
                  required: true,
              },
             
              txtConfirmPassword: {
                  required: true, equalTo: "#txtpassInp"
              },
             

              chkTerms: {
                  required: true
              }
          },
          highlight: function (element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
          },
          unhighlight: function (element) {
              $(element).closest('.form-group').removeClass('has-error').addClass('has-success ');
          },
          errorClass: 'help-block',
          errorPlacement: function errorPlacement(element, e) {
              return true;
          },
          submitHandler: function (form) {  // do other things for a valid form  

          },
      })
);
function clear() {

    $('#txtemailInp').val("");
    $('#txtpassInp').val("");
    $('#txtConfirmPassword').val("");
    $('#txtPhoneInp').val("");
    document.getElementsByName('chkTerms').checked = false;

    
}
$("#btnRegis").on("click", function () {
    SaveUserDetails();

});





var SaveUserDetails = function () {

    debugger;   
    var UserName = $('#txtemailInp').val();
    var Password = $('#txtpassInp').val();
    var PhoneNumber = $('#txtPhoneInp').val();
   
    var request =
    {
        Email: UserName,
        Password: Password,
        ConfirmPassword: Password,
        PhoneNumber: PhoneNumber
    };

    $.ajax({
        type: "POST",
        url: RootUrl + "/api/AccountApi/Register",
        datatype: 'JSON',
        data: JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger;
            if (data=="true") {

                clear();
                Swal.fire({
                    title: 'Data has been registered successfully',
                    text: "Please confirm your mail",
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    //cancelButtonColor: '#d33',
                    //confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        pageUrl = RootUrl + "LoginPage";
                        window.location.href = pageUrl;
                    }
                });
                
                //swal({
                //    title: 'You have been registered successfully...',
                //    text: "Move to Login!",
                //    type: 'success',
                //    confirmButtonColor: '#3085d6',

                //    confirmButtonText: 'ok!'
                //}).then(function () {
                //    // if (result) {

                //    pageUrl = RootUrl + "Account/Login";
                //    window.location.href = pageUrl;

                //});
            }
            else {
                swal("", data.Message, "error");
            }
        },

      
    });
}
/// <summary>