﻿$(document).ready(

    getId()
);
function getId() {
    debugger;
    var url = window.location.pathname;
   // var id = url.substring(url.lastIndexOf('/') + 1);
    var res = url.split("/");
    Id = res[1];
    var rUrl = RootUrl + "/api/AccountApi/ConfirmMail?Id=" + Id;
    var Token = JSON.parse(localStorage.getItem('token')) || [];

    $.ajax({
        type: 'GET',
        url: rUrl,
      //  headers: { Authorization: "bearer " + Token },
        datatype: 'JSON',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data != "false") {
                Swal.fire({
                    title: 'Your Email Has Been Confirmed',
                    text: "Please click here to login",
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    //cancelButtonColor: '#d33',
                    //confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        pageUrl = RootUrl + "LoginPage";
                        window.location.href = pageUrl;
                    }
                });
            } else {
                Swal.fire({
                    title: 'Your Email Has Not Been Confirmed',
                    text: "Please Check your mail",
                    icon: 'Error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    //cancelButtonColor: '#d33',
                    //confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        pageUrl = RootUrl + "LoginPage";
                        window.location.href = pageUrl;
                    }
                });
            }
            
        },
        error: function (response) {
            console.log(response);
        },

    });
}