﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
//using AspCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repo.Models.Context;
using Repo.Repository;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.Extensions.Logging;
using DinkToPdf.Contracts;
using DinkToPdf;
using AspCore.Data;
using AspCore.Data.Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using AspCore.Services;
using AspCore.Entities;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using StackifyLib;
//using NLog.Extensions.Logging;
using NLog;

//using Microsoft.Extensions.Hosting;
namespace AspCore
{
    public class Startup
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IHostingEnvironment _hostingEnvironment;
        public Startup(IConfiguration configuration,IHostingEnvironment env)
        {
           // _appConfiguration = env.GetAppConfiguration();
            _hostingEnvironment = env;
            Configuration = configuration;
            string userName = Configuration.GetSection("AppConfiguration")["UserName"];
            string password = Configuration.GetSection("AppConfiguration")["Password"];
            var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
        .AddEnvironmentVariables();
            Configuration = builder.Build();


            StackifyLib.Config.Environment = env.EnvironmentName; //optional
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileProvider>(
            new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));


            var architectureFolder = (IntPtr.Size == 8) ? "64 bit" : "32 bit";
            var wkHtmlToPdfPath = Path.Combine(_hostingEnvironment.ContentRootPath, $"wkhtmltox\\v0.12.4\\{architectureFolder}\\libwkhtmltox");
            //  var wkHtmlToPdfPath = Path.GetFullPath($"C:\\Program Files\\wkhtmltox\v0.12.4{ architectureFolder}\\libwkhtmltox");

            CustomAssemblyLoadContext cont = new CustomAssemblyLoadContext();
            cont.LoadUnmanagedLibrary(wkHtmlToPdfPath);
            // cont.LoadUnmanagedLibrary(Path.GetFullPath(@"C:\Program Files\wkhtmltox\v0.12.4\64 bit\libwkhtmltox.dll"));

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            


            services.AddMvc();
            services.AddDbContext<SampleDbContext>(item => item.UseSqlServer(Configuration.GetConnectionString("myconn")));

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
           
            services.AddTransient<ICustumer, CustumerRepository>();
            services.AddTransient<IChat, ChatRepository>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]));
           // services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            services.AddDefaultIdentity<ApplicationUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //--------------------------------------------------------------//

            // services.AddTransient<Services.IEmailSender, EmailSender>();
            services.AddOptions();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.AddSingleton<Services.IEmailSender, EmailSender>();


           

            services.Configure<AuthMessageSenderOptions>(Configuration);
            services.Configure<DataProtectionTokenProviderOptions>(o =>
      o.TokenLifespan = TimeSpan.FromHours(12));

            services.ConfigureApplicationCookie(o => {
                o.ExpireTimeSpan = TimeSpan.FromDays(5);
                o.SlidingExpiration = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Account/Login";
                //options.LogoutPath = $"/Account/Logout";
                //options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
           .AddJwtBearer(options =>
           {
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidateLifetime = true,
                   ValidateIssuerSigningKey = true,

                   ValidIssuer = _hostingEnvironment.WebRootPath,
                   ValidAudience = _hostingEnvironment.WebRootPath,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("YourKey-2374-OFFKDI940NG7:56753253-tyuw-5769-0921-kfirox29zoxv"))
               };
           });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR(
                options =>
                {
                    options.KeepAliveInterval = TimeSpan.Zero;
                    options.EnableDetailedErrors = true;
                  //  options.KeepAliveInterval = TimeSpan.FromMinutes(10);
                });
            

            services.AddCors(options => options.AddPolicy("CorsPolicy",
            builder =>
            {
                builder.AllowAnyMethod().AllowAnyHeader()
                       .WithOrigins(_hostingEnvironment.WebRootPath)
                       .AllowCredentials();
            }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IHostingEnvironment env,  ILoggerFactory loggerFactory)
        //{
        //    loggerFactory.AddConsole(Configuration.GetSection("Logging"));
        //    loggerFactory.AddDebug();

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            // app.UseRouting();
            app.UseSignalR(route =>
            {
                route.MapHub<ChatHub>("/chathub");
            });
            app.UseCors("CorsPolicy");


            app.ConfigureStackifyLogging(Configuration);
            // loggerFactory.AddStackify(); //add the provider
           //loggerFactory.AddDebug();

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            
           // loggerFactory.AddLog4Net();
            app.UseAuthentication();


            //var config = new NLog.Config.LoggingConfiguration();
            //var logfile = new NLog.Targets.FileTarget("logfile") { FileName = "Logs.txt" };
            //var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            //config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
            //config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);

          

            //Enable ASP.NET Core features (NLog.web) - only needed for ASP.NET Core users
           

            var options = new RewriteOptions()
            .Add(new EmployeeRedirectRule());

            

            app.UseMvc(routes =>
            {
                var rewrite = new RewriteOptions()

                .AddRewrite(@"RegisterPage", "Account/Register", skipRemainingRules: false)
                .AddRewrite(@"SignalRChat", "Home/SignalR", skipRemainingRules: false)
                .AddRewrite(@"^useId=$code=$ResetPasswordPage", "Account/ResetPassword", skipRemainingRules: false)
                .AddRewrite(@"LoginPage", "Account/Login", skipRemainingRules: false)
                                .AddRewrite(@"ConfirmMailPage", "Account/ConfirmMail", skipRemainingRules: false)
                                .AddRewrite(@"HomePage", "Home/Index", skipRemainingRules: false)
                                .AddRewrite(@"^ConfirmMailPage", "Account/ConfirmMail", skipRemainingRules:false)
                 .AddRewrite(@"SignalRWhatsApp", "Home/SignalRWhatsApp", skipRemainingRules: false);

                app.UseRewriter(rewrite);

                
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}"
                    );
                routes.MapRoute(
                   name: "SignalRChat",
                   template: "{controller=Home}/{action=SignalR}"
                   );
                routes.MapRoute(
                  name: "SignalRWhatsApp",
                  template: "{controller=Home}/{action=SignalRWhatsApp}"
                  );
                routes.MapRoute(
                   name: "RegisterPage",
                   template: "{controller=Account}/{action=Register}"
                   );
                routes.MapRoute(
                   name: "LoginPage",
                   template: "{controller=Account}/{action=Login}"
                   );
                routes.MapRoute(
                   name: "HomePage",
                   template: "{controller=Home}/{action=Index}"
                   );
                routes.MapRoute(
                   name: "ForgotPasswordPage",
                   template: "{controller=Account}/{action=ForgotPassword}"
                   );
                routes.MapRoute(
                   name: "ConfirmMailPage",
                   template: "{controller=Account}/{action=ConfirmMail}"
                   );
                routes.MapRoute(
                   name: "ResetPasswordPage",
                   template: "{controller=Account}/{action=ResetPassword}"
                   );
            });

            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Account}/{action=Login}/{id?}");
            //});
        }
    
    }
}
