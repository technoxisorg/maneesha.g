﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AspCore.Data.Models
{
    
        public class ApplicationUser : IdentityUser
        {

        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser> manager)

        {

            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);

            identity.AddClaim(new Claim(ClaimTypes.Name, this.UserName));
            return identity;
            
        }
        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    // Add custom user claims here
        //    return userIdentity;
        //}


        //public virtual Custumer Custumer { get; set; }
        // Your Extended Properties
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public int ClientId { get; set; }
        //public string UserImage { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime? ModifiedDate { get; set; }
        //public string StatusUpdatedBy { get; set; }
        //public DateTime? StatusUpdatedOn { get; set; }
        //public bool IsActive { get; set; }
        //public string UserName { get; set; }
        //public string Nor { get; set; }
        //public string UserImagePath { get; set; }

    }

   
       
        //public class Custumer
        //{
       
        //public int Id { get; set; }

        //[Key, ForeignKey("User")]
        //public string  CustumerId { get; set; }
        //public string ContactName { get; set; }
        //    public string ContactTitle { get; set; }
        //    public string Address { get; set; }
        //    public string City { get; set; }
        //    public string Region { get; set; }
        //    public string Country { get; set; }
        //    public string Phone { get; set; }
        //}
    
    
}
