﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCore.Data.Models
{
    public partial class AspNetUserRoles
    {

        [Key]
        public string UserId { get; set; }

        public string RoleId { get; set; }

        //public AspNetRoles Role { get; set; }
       // public AspNetUsers User { get; set; }
    }
}
