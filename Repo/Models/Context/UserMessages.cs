﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repo.Models.Context
{
    public class UserMessages
    {
        public int Id { get; set; }
        public string msgFrom { get; set; }
        public string msgTo { get; set; }
        public string fileName { get; set; }
        public string fileType { get; set; }
        public string filePath { get; set; }
        public string groupName { get; set; }
        public string message { get; set; }
        public DateTime Date { get; set; }
        public bool haveSeen { get; set; }
        public bool haveSeenReceiver { get; set; }
    }
}
